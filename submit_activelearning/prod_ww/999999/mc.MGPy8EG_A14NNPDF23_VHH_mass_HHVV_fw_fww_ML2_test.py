# modified from https://its.cern.ch/jira/browse/ATLMCPROD-8381 or https://its.cern.ch/jira/browse/ATLMCPROD-9024 or https://its.cern.ch/jira/browse/ATLMCPROD-9346: they are all same, parameter values are parametrised by the JO file name

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

gridpack_mode=False
gridpack_dir=None
evgenConfig.nEventsPerJob = 2000

# parametrised JO from bash env variables
fw = float(os.environ['envPARAM_FW'])
fww = float(os.environ['envPARAM_FWW'])
Mass = float(os.environ['envPARAM_MASS'])
print 'parameters: mass= %d, fw= %s, fww= %s' % (Mass,fw,fww)

bonus_file = open('pdg_extras.dat','w')
#  The most important number is the first: the PDGID of the particle
bonus_file.write('254\n')
bonus_file.close()
testSeq.TestHepMC.UnknownPDGIDFile = 'pdg_extras.dat'
#---------------------------------------------------------------------------------------------------
# Setting EFT parameters in HH model 1000,0
#---------------------------------------------------------------------------------------------------
paramdim6coeff={
        'rhoH':  '5.000000e-02',
        'Lambda':  '5.000000e+03',
        'fW':  fw,
	  'fWW':  fww,
	  'fB':  '0.000000e+00',
	  'fBB':  '0.000000e+00'
    }
#---------------------------------------------------------------------------------------------------
# Setting X0 mass and width for param_card.dat
#---------------------------------------------------------------------------------------------------
resonanceMass = Mass
HHMass  = {'254':'%e # MHH'%resonanceMass}  #Mass
HHDecay = {'254':'DECAY 254 Auto # WHH'} #Width


#---------------------------------------------------------------------------------------------------
# Generating pp -> VX0, X0 -> VV in HC LO model
#---------------------------------------------------------------------------------------------------
process = """
import model SMwithHeavyScalarDim4Dim6_NoDecay_UFO
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define v = z w- w+
generate p p > hh > v v v
output -f
"""
process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Setting the number of generated events to 'safefactor' times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor = 25.0
nevents    = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob

settings = {'lhe_version'   : '3.0', 
          'cut_decays'    : 'F',
	    'nevents'    :int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

params={}
params['DIM6COEFF'] = paramdim6coeff
params['MASS']=HHMass
params['DECAY']=HHDecay
modify_param_card(process_dir=process_dir,params=params)

madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
   os.unlink(madspin_card)
mscard = open(madspin_card,'w') 
mscard.write("""#************************************************************                                                                               

#*                        MadSpin                           *                                                                                                              
#*                                                          *                                                                                                              
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                                                                                                              
#*                                                          *                                                                                                              
#*    Part of the MadGraph5_aMC@NLO Framework:              *                                                                                                              
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                                                                                                              
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                                                                                                              
#*                                                          *                                                                                                              
#************************************************************                                                                                                              
#Some options (uncomment to apply)                                                                                                                                         
#                                                                                                                                                                          
# set seed 1                                                                                                                                                               
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight                                                                                     
# set BW_cut 15                # cut on how far the particle can be off-shell                                                                                              
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event                                                                                        
#                                                                                                                                                                          
set seed %i                                                                                                                                                                
# specify the decay for the final state particles                                                                                                                          
# decay t > w+ b, w+ > all all                                                                                                                                               
# decay z > all all                                                                                                                                                          
decay z > all all                                                                                                                                                         
decay w- > all all                                                                                                                                                    
decay w+ > all all                                                                                                                                                       
# running the actual code                                                                                                                                                  
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

generate(runArgs=runArgs,process_dir=process_dir)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True) 

#######
import os
if 'ATHENA_PROC_NUMBER' in os.environ:
   print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
   njobs = os.environ.pop('ATHENA_PROC_NUMBER')
   #Try to modify the opts underfoot
   if not hasattr(opts,'nprocs'): print 'Did not see option!'
   else: opts.nprocs = 0
   print opts


#### Shower

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'pp->VHH, HH->VV with LO HH model'
evgenConfig.keywords+=['BSM', "Higgs"]
evgenConfig.contact = ['Rui Zhang <rui.zhang@cern.ch>']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

### Set lepton filters
if not hasattr(filtSeq, "MultiLeptonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
   lepfilter = MultiLeptonFilter("lepfilter")
   filtSeq += lepfilter
if not hasattr(filtSeq, "LeptonPairFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import LeptonPairFilter
   lepPairfilter = LeptonPairFilter("lepPairfilter")
   filtSeq += lepPairfilter

filtSeq.lepfilter.Ptcut = 18000.0 #MeV
filtSeq.lepfilter.Etacut = 2.7
filtSeq.lepfilter.NLeptons = 2 #minimum

filtSeq.lepPairfilter.NSFOS_Max = -1
filtSeq.lepPairfilter.NSFOS_Min = -1
filtSeq.lepPairfilter.NSFSS_Min = 0
filtSeq.lepPairfilter.NOFSS_Min = 0
filtSeq.lepPairfilter.NPairSum_Min = 1
filtSeq.lepPairfilter.UseSFSSInSum = True
filtSeq.lepPairfilter.UseOFSSInSum = True
filtSeq.lepPairfilter.Ptcut = 18000.0 #MeV
filtSeq.lepPairfilter.Etacut = 2.7
filtSeq.lepPairfilter.NLeptons_Min = 2
