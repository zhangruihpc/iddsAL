## Reana examples using PanDA

### Setup
On lxplus
```
setupATLAS
lsetup panda
```

### Set panda secrets
Reana uses EVN variable `$REANA_WORKON` to store your personable access token.
We can tell PanDA this token so that PanDA can submit Reana tasks on behalf of you ([new feature in PanDA](https://panda-wms.readthedocs.io/en/latest/client/secret.html)).
It is possible to provide a file as a secret, eg your grid certificate.
```
pbook
> set_secret('REANA_WORKON', 'your_token_string')
> set_secret('usercert.pem', '/path/to/usercert.pem', is_file=True)
> list_secrets() # check if the secret is setup successfully
```
Then in `prun` submission command, add `--useSecrets` so that PanDA will create a local file called `panda_secrets.json` on the PanDA node where the job runs.
One can use python script to retrieve it.
String secrets are exposed as environment variables.
```
import json
with open('panda_secrets.json') as f:
   secrets = json.load(f)
   do_something_with_a_secret(secrets['MY_SECRET'])
```
or one can set SHELL EVN variables using
```
$(cat panda_secrets.json | jq -r 'keys[] as $k | "export \($k)=\(.[$k])"')
```

### Read input from EOS
To grand REANA server access to your EOS, follow the [REANA doc](https://docs.reana.io/advanced-usage/storage-backends/eos/).
You need to create `keytab` to access EOS from Reana and add it as a panda secret.
<details><summary>How to create keytab</summary>
<p>
On lxplus
```
cern-get-keytab --user --keytab <keytab_filename> --login <lxplus_username>
```
It will require an input of your lxplus password.
Then it will generate a <keytab_filename> file in the current folder.
</p>
</details>


Then submit an example PanDA job:
```
cd panda_my_recast_eos
source submit.sh
```
This will run [this example](https://gitlab.cern.ch/zhangruihpc/reana-demo-atlas-recast).

### Get input via Rucio
To grand REANA server access Rucio, follow the [REANA doc](https://docs.reana.io/advanced-usage/access-control/voms-proxy/).
You need to upload valid Grid certificate to PanDA, namely `userkey.pem` and `usercert.pem` files in `~/.globus/`.
<details><summary>How to get Grid Certificate</summary>
<p>
- Go to [https://ca.cern.ch/ca/](https://ca.cern.ch/ca/).
- Click on `New Grid User certificate`.
- Click on button `Get Grid User Certificate`.
- Click on `Download certificate`.
</p>
</details>


Then submit an example PanDA job:
```
cd panda_my_recast_rucio
source submit.sh
```
This will run [this example](https://gitlab.cern.ch/zhangruiForked/reana-demo-atlas-recast-2).
