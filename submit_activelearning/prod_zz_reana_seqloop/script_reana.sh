#!/bin/bash
# ref: https://gitlab.cern.ch/recast-atlas/exotics/ana-exot-2018-046/-/tree/reana_working

loop_counter=$1
sliced_myparamMZD=$2
sliced_myparamEPSILON=$3
deriva1=$4
job_name=ZX${i}_${sliced_myparamMZD}_${sliced_myparamEPSILON}

#job_name=$1
job_name=${job_name//./p}
job_name=${job_name//-/n}
echo "job_name=$job_name"
echo "deriva1=$deriva1"
echo "all=${*:2}"

### setup secrets via panda secrets
if [ -z "$REANA_ACCESS_TOKEN" ]; then
    $(cat panda_secrets.json | jq -r 'keys[] as $k | "export \($k)=\(.[$k])"')
fi

example=recast-zz
git clone https://gitlab.cern.ch/zhangruihpc/reana-zz.git ${example}
cd ${example}

#### Before Christian could fix the reana payload issue, let's use default sample
## Update reana.yaml with corresponding dataset
\cp ../reana-template.yaml reana.yaml
sed -i "s|VAR_INPUT|$deriva1|g" reana.yaml
#####

echo "===== debug ====="
echo cat reana.yaml
cat reana.yaml
echo "===== debug ====="

# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name

# http://docs.reana.io/advanced-usage/access-control/voms-proxy/
echo "setup proxy for both rucio and eos usage; you may only need one according to your demand"
# for rucio usage
reana-client secrets-add --overwrite --file ../userkey.pem --file ../usercert.pem --env VOMSPROXY_PASS=emhhbmdydWkK --env VONAME=atlas
# for eos usage
reana-client secrets-add --overwrite --env CERN_USER=zhangr --env CERN_KEYTAB=keytab --file ../keytab

# delete the pem file from grid site after transferring to reana
echo "delete secrete"
rm -f ../keytab ../usercert.pem ../userkey.pem

echo "upload reana"
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
echo "reana start"
reana-client start
## ... should be finished in about a minute
echo "reana status"
reana-client status --format NAME,RUN_NUMBER,STATUS
echo "get job_num immediately after submission in case other jobs submit a task with the same job_name"
job_num=`reana-client status --format NAME,RUN_NUMBER,STATUS | tr -s ' ' | cut -d " " -f 2 | sed -n 2p`
task_name=${job_name}.${job_num}
echo 'task_name' $task_name
# list workspace files
echo "reana ls"
reana-client ls

cd ../

# wait and download output results
echo waiting and download output results
status=`reana-client status | python -c $'import sys\nfor line in sys.stdin:print(line.split()[-2])' | sed -n 2p`
echo status: $status
reana-client status
sleep 60
while [[ $status != 'finished' ]]; do
    sleep 60
    status=`reana-client status --format NAME,RUN_NUMBER,STATUS | tr -s ' ' | cut -d " " -f 3 | sed -n 2p`
    echo new status: $status
    if [[ $status == 'failed' ]]; then
        break
    fi
done
echo "reana download"
reana-client download -w ${task_name}
echo {\"loop\": ${loop_counter}, \"myparamMZD\": ${sliced_myparamMZD}, \"myparamEPSILON\": ${sliced_myparamEPSILON}, \"DS\": \"$deriva1\"} > limit_stage/input_param.json
ls limit_stage
cat limit_stage/input_param.json
tar -czvf out-rucio-2.tar limit_stage/

# cleanup
rm -fr ${example}
