These tasks require athena. Setup Athena first before submission:
```
asetup Athena,master,latest
```
And in `--pchain` command, use `--useAthenaPackages`.
