cwlVersion: v1.0
class: Workflow

inputs:
  param_myparamMZD:
    type: float
    default: 30
  param_myparamEPSILON:
    type: float
    default: 0.0001

outputs:
  outDS:
    type: string
    outputSource: inner_work_bottom/outDS

steps:
  inner_work_evgen:
    run: prun
    in:
      opt_exec:
        default: "export JOBOPTSEARCHPATH=$PWD:$JOBOPTSEARCHPATH; export envPARAM_MZD=%{myparamMZD}; export envPARAM_EPSILON=%{myparamEPSILON}; Gen_tf.py --maxEvents=%MAXEVENTS --skipEvents=0 --ecmEnergy=13000 --firstEvent=%FIRSTEVENT:0 --outputEVNTFile=EVNT.pool.root.1 --randomSeed=%RNDM:100 --runNumber=999999 --AMITag=e4551 --jobConfig=999999; unset envPARAM_MZD envPARAM_EPSILON"
      opt_args:
        default: "--outputs 'EVNT.pool.root.1' --nEvents=100 --nJobs 10 --athenaTag AthGeneration,21.6.92 --noBuild --expertOnly_skipScout --avoidVP"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_simul:
    run: prun
    in:
      opt_inDS: inner_work_evgen/outDS
      opt_exec:
        default: "Sim_tf.py --inputEVNTFile=%IN --maxEvents=%MAXEVENTS --postInclude default:RecJobTransforms/UseFrontier.py --preExec 'EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)' 'EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True' --preInclude EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py --skipEvents=%SKIPEVENTS --firstEvent=%FIRSTEVENT:0 --outputHITSFile=HITS.pool.root.1 --physicsList=FTFP_BERT_ATL_VALIDATION --randomSeed=%RNDM:1 --DBRelease=all:current --conditionsTag default:OFLCOND-MC16-SDR-14 --geometryVersion=default:ATLAS-R2-2016-01-00-01_VALIDATION --runNumber=999999 --AMITag=a875 --DataRunNumber=284500 --simulator=ATLFASTII --truthStrategy=MC15aPlus"
      opt_args:
        default: "--outputs 'HITS.pool.root.1' --nEventsPerFile=10 --nEventsPerJob 10 --athenaTag Athena,21.0.31 --noBuild --expertOnly_skipScout --avoidVP"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_reco:
    run: prun
    in:
      opt_inDS: inner_work_simul/outDS
      opt_exec:
        default: "Reco_tf.py --inputHITSFile=%IN --maxEvents=%MAXEVENTS --postExec 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"]' 'ESDtoAOD:fixedAttrib=[s if \"CONTAINER_SPLITLEVEL = '\"'\"'99'\"'\"'\" not in s else \"\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' --postInclude default:PyJobTransforms/UseFrontier.py --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODSLIM\");' 'all:from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinInDetForwardTrackParticles.set_Value_and_Lock(True)' --preInclude HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_run300000_mc16d.py --skipEvents=%SKIPEVENTS --autoConfiguration=everything --conditionsTag default:OFLCOND-MC16-SDR-20 --geometryVersion=default:ATLAS-R2-2016-01-00-01 --runNumber=999999 --digiSeedOffset1=%RNDM:1 --digiSeedOffset2=%RNDM:1 --digiSteeringConf=StandardSignalOnlyTruth --AMITag=r10201 --steering=doRDO_TRIG --inputHighPtMinbiasHitsFile=%IN_MINH --inputLowPtMinbiasHitsFile=%IN_MINL --numberOfCavernBkg=0 --numberOfHighPtMinBias=0.2099789464 --numberOfLowPtMinBias=80.290021063135 --pileupFinalBunch=6 --outputAODFile=AOD.pool.root.1 --jobNumber=%RNDM:1 --triggerConfig=RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2179,51,207 --valid=True "
      opt_args:
        default: "--outputs 'AOD.pool.root.1' --nEventsPerFile=10 --nEventsPerJob 10 --athenaTag Athena,21.0.53 --noBuild --expertOnly_skipScout --avoidVP --secondaryDSs IN_MINH:1:mc16_13TeV.361239.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_high.simul.HITS.e4981_s3087_s3111/,IN_MINL:1:mc16_13TeV.361238.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_low.simul.HITS.e4981_s3087_s3111/ --notExpandSecDSs --forceStaged --forceStagedSecondary"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_deriv:
    run: prun
    in:
      opt_inDS: inner_work_reco/outDS
      opt_exec:
        default: "Reco_tf.py --inputAODFile=%IN --athenaMPMergeTargetSize 'DAOD_*:0' --preExec 'default:rec.doApplyAODFix.set_Value_and_Lock(True); from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-47\"; from AthenaCommon.AlgSequence import AlgSequence; topSequence = AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\"InDetTrackParticlesFixer\", Containers = [ \"InDetTrackParticlesAux.\" ] );from AthenaMP.AthenaMPFlags import jobproperties as ampjp;ampjp.AthenaMPFlags.UseSharedWriter=True;import AthenaPoolCnvSvc.AthenaPool;ServiceMgr.AthenaPoolCnvSvc.OutputMetadataContainer=\"MetaData\"' --sharedWriter True --runNumber 999999 --AMITag p3870 --outputDAODFile pool.root.1 --reductionConf HIGG2D1 --passThrough True"
      opt_args:
        default: "--outputs 'DAOD_HIGG2D1.pool.root.1' --nEventsPerFile 10 --nEventsPerJob 10 --athenaTag AthDerivation,21.2.64.0 --noBuild --expertOnly_skipScout --avoidVP --forceStaged"
      opt_useAthenaPackages:
        default: true
    out: [outDS]


  inner_work_reana:
    run: reana
    in:
      opt_inDS: inner_work_deriv/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-submitter
      opt_exec:
        default: "source script_rucio.sh pchain_ZZ %{DS0} "
      opt_args:
        default: "--site CERN --outputs out-rucio-2.tar --useSecrets"
    out: [outDS] 


  checkpoint:
    run: junction
    in:
      opt_inDS: inner_work_reana/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/steeringcontainer:0.0.7
      opt_exec:
        default: "echo DS0aaaa %{DS0} DS1aaaa %{DS1}; bash script_junction.sh %{i} %{DS0} %{myparamMZD} %{myparamEPSILON}"
      opt_args:
        default: "--site CERN --outputs 'results.json,history.json' --persistentFile history.json --forceStaged"
    out: [outDS]

