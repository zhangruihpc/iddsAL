#asetup AthGeneration,21.6.92,here

#Gen_tf.py --ecmEnergy=13000. --maxEvents=10 --firstEvent=1 --randomSeed=732595 --outputEVNTFile=EVNT.root  --jobConfig=999999 

#pchain --cwl evnt_to_deriv2.cwl --yaml dummy.yaml --useAthenaPackages --outDS user.$USER.prod_Z32 #--check # this 32 and evnt_to_deriv2.cwl worked

#pchain --cwl evnt_to_deriv3.cwl --yaml dummy.yaml --useAthenaPackages --outDS user.$USER.prod_Z40 #--check

#pchain --cwl evnt_to_deriv4.cwl --yaml dummy.yaml --useAthenaPackages --outDS user.$USER.prod_Z70

#pchain --cwl evnt_to_deriv.cwl --yaml dummy.yaml --useAthenaPackages --outDS user.$USER.prod_Z41
pchain --cwl evnt_to_deriv_large.cwl --yaml dummy.yaml --useAthenaPackages --outDS user.$USER.prod_Z40
