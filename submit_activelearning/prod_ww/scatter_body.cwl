cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  param_myparamFW:
    type: float[]
    #default: [-690, -1160, 860, -540, -600, -900, -2050, 560, 1510, 380]
    #default: [0, 0, 1100]
    #default: [-1200, 470, 1200]
    default: [0]

  param_myparamFWW:
    type: float[]
    #default: [14240, 11600, 11550, 10960, 12880, 12270, -2420, 10590, 11840, 14520]
    #default: [0, 0, -2200]
    #default: [2400, 7050, -2400]
    default: [0]

  param_myparamMASS:
    type: float[]
    #default: [290, 290, 310, 300, 270, 300, 950, 280, 1410, 290]
    #default: [300, 360, 300]
    #default: [300, 300, 600]
    default: [300]
  BKGH: string

outputs:
  outDS:
    type: string
    outputSource: checkpoint/outDS


steps:
  parallel_work:
    #run: loop_main.cwl
    run: loop_main_small.cwl
    scatter: [param_sliced_myparamFW, param_sliced_myparamFWW, param_sliced_myparamMASS]
    scatterMethod: dotproduct
    in:
      param_sliced_myparamFW: param_myparamFW
      param_sliced_myparamFWW: param_myparamFWW
      param_sliced_myparamMASS: param_myparamMASS
      BKGH: BKGH
    out: [outDS]


  checkpoint:
    run: junction
    in:
      opt_inDS: parallel_work/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/steeringcontainer:0.0.7
      opt_exec:
        default: "echo pwd; pwd; echo ls; ls; echo ls *; ls *; echo ls ../*; ls ../*; echo current=%{i}; touch results.json history.json"
      opt_args:
        default: "--site CERN --outputs 'results.json,history.json' --persistentFile history.json --forceStaged"
    out: [outDS]

