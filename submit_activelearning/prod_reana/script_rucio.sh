#!/bin/bash
# ref: https://gitlab.cern.ch/recast-atlas/exotics/ana-exot-2018-046/-/tree/reana_working

job_name=$1
deriva1=$2
pileup2=$3
deriva3=$4
pileup4=$5
deriva5=$6
pileup6=$7
echo "job_name=$job_name"
echo "deriva1=$deriva1"
echo "pileup2=$pileup2"
echo "deriva3=$deriva3"
echo "pileup4=$pileup4"
echo "deriva5=$deriva5"
echo "pileup6=$pileup6"
echo "all=${*:2}"

### setup secrets via panda secrets
if [ -z "$REANA_ACCESS_TOKEN" ]; then
    $(cat panda_secrets.json | jq -r 'keys[] as $k | "export \($k)=\(.[$k])"')
fi

example=recast-rucio
git clone https://gitlab.cern.ch/zhangruihpc/reana-ana-exot-2018-046.git ${example}
cd ${example} && git reset --hard 203f1f16b80cec7ad76effb54ca4087deefec872

#####
## No update in reana.yaml for the moment since we will use the eos dataset for demonstration
#\cp ../reana-template.yaml reana.yaml
#
#sed -i "s|VAR_INPUT_MC16A|$deriva1|g" reana.yaml
#sed -i "s|VAR_INPUT_MC16D|$deriva3|g" reana.yaml
#sed -i "s|VAR_INPUT_MC16E|$deriva5|g" reana.yaml
#sed -i "s|VAR_PILE_MC16A|$pileup2|g" reana.yaml
#sed -i "s|VAR_PILE_MC16D|$pileup4|g" reana.yaml
#sed -i "s|VAR_PILE_MC16E|$pileup6|g" reana.yaml
#####

echo "===== debug ====="
git status
ls *
ls ../
echo cat reana.yaml
cat reana.yaml
echo "===== debug ====="

# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name

# http://docs.reana.io/advanced-usage/access-control/voms-proxy/
echo "setup proxy for both rucio and eos usage; you may only need one according to your demand"
# for rucio usage
reana-client secrets-add --overwrite --file ../userkey.pem --file ../usercert.pem --env VOMSPROXY_PASS=emhhbmdydWkK --env VONAME=atlas
# for eos usage
reana-client secrets-add --overwrite --env CERN_USER=zhangr --env CERN_KEYTAB=keytab --file ../keytab

# delete the pem file from grid site after transferring to reana
echo "delete secrete"
rm -f ../keytab ../usercert.pem ../userkey.pem

echo "upload reana"
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
echo "reana start"
reana-client start
## ... should be finished in about a minute
echo "reana status"
reana-client status
# list workspace files
echo "reana ls"
reana-client ls

cd ../

# wait and download output results
echo waiting and download output results
status=`reana-client status | python -c $'import sys\nfor line in sys.stdin:print(line.split()[-2])' | sed -n 2p`
echo status: $status
reana-client status
sleep 60
#while [[ $status != 'finished' ]]; do
#    sleep 60
#    status=`reana-client status | python -c $'import sys\nfor line in sys.stdin:print(line.split()[-2])' | sed -n 2p`
#    echo new status: $status
#    if [[ $status == 'failed' ]]; then
#        break
#    fi
#done
echo "reana download"
reana-client download
tar -czvf out-rucio-2.tar  recast-myrun

echo ----- debug ----
pwd
ls
echo ----- debug ----

# cleanup
rm -fr ${example}
