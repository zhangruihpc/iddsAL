cwlVersion: v1.0
class: Workflow

inputs:
  param_myparamMZD:
    type: float
    default: 30
  param_myparamEPSILON:
    type: float
    default: 0.0001

outputs:
  outDS:
    type: string
    outputSource: inner_work_bottom/outDS

steps:
  inner_work_evgen:
    run: prun
    in:
      opt_exec:
        default: "export JOBOPTSEARCHPATH=$PWD:$JOBOPTSEARCHPATH; export envPARAM_MZD=%{myparamMZD}; export envPARAM_EPSILON=%{myparamEPSILON}; Generate_tf.py --maxEvents=%MAXEVENTS --skipEvents=0 --ecmEnergy=13000 --firstEvent=%FIRSTEVENT:1 --outputEVNTFile=EVNT.pool.root.1 --randomSeed=%RNDM:100 --runNumber=343237 --AMITag=e4551 --jobConfig=343237/MC15.343237.MadGraphPythia8EvtGen_A14NNPDF23LO_HAHMggfZZd4l_mZd30.py; unset envPARAM_MZD envPARAM_EPSILON"
      opt_args:
        default: "--disableAutoRetry --outputs 'EVNT.pool.root.1' --nEvents=1000 --nJobs 20 --athenaTag AtlasProduction,19.2.5.15 --noBuild --expertOnly_skipScout --avoidVP"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_simul:
    run: prun
    in:
      opt_inDS: inner_work_evgen/outDS
      opt_exec:
        default: "Sim_tf.py --inputEVNTFile=%IN --maxEvents=%MAXEVENTS --postInclude default:RecJobTransforms/UseFrontier.py --preExec 'EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)' 'EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True' --preInclude EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py --skipEvents=%SKIPEVENTS --firstEvent=%FIRSTEVENT:1 --outputHITSFile=HITS.pool.root.1 --physicsList=FTFP_BERT_ATL_VALIDATION --randomSeed=%RNDM:100 --DBRelease=all:current --conditionsTag default:OFLCOND-MC16-SDR-14 --geometryVersion=default:ATLAS-R2-2016-01-00-01_VALIDATION --runNumber=343237 --AMITag=a875 --DataRunNumber=284500 --simulator=ATLFASTII --truthStrategy=MC15aPlus"
      opt_args:
        default: "--disableAutoRetry --outputs 'HITS.pool.root.1' --nEventsPerFile=50 --nEventsPerJob 50 --athenaTag Athena,21.0.31 --noBuild --expertOnly_skipScout --avoidVP"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_reco:
    run: prun
    in:
      opt_inDS: inner_work_simul/outDS
      opt_exec:
        default: "Reco_tf.py --inputHITSFile=%IN --maxEvents=%MAXEVENTS --postExec 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"]' 'ESDtoAOD:fixedAttrib=[s if \"CONTAINER_SPLITLEVEL = '\"'\"'99'\"'\"'\" not in s else \"\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' 'RDOtoRDOTrigger:conddb.addOverride(\"/CALO/Ofl/Noise/PileUpNoiseLumi\",\"CALOOflNoisePileUpNoiseLumi-mc15-mu30-dt25ns\")' 'ESDtoAOD:CILMergeAOD.removeItem(\"xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.LATERAL.LONGITUDINAL.SECOND_R.SECOND_LAMBDA.CENTER_MAG.CENTER_LAMBDA.FIRST_ENG_DENS.ENG_FRAC_MAX.ISOLATION.ENG_BAD_CELLS.N_BAD_CELLS.BADLARQ_FRAC.ENG_BAD_HV_CELLS.N_BAD_HV_CELLS.ENG_POS.SIGNIFICANCE.CELL_SIGNIFICANCE.CELL_SIG_SAMPLING.AVG_LAR_Q.AVG_TILE_Q.EM_PROBABILITY.PTD.BadChannelList\");CILMergeAOD.add(\"xAOD::CaloClusterAuxContainer#CaloCalTopoClustersAux.N_BAD_CELLS.ENG_BAD_CELLS.BADLARQ_FRAC.AVG_TILE_Q.AVG_LAR_Q.CENTER_MAG.ENG_POS.CENTER_LAMBDA.SECOND_LAMBDA.SECOND_R.ISOLATION.EM_PROBABILITY\");StreamAOD.ItemList=CILMergeAOD()' --postInclude default:PyJobTransforms/UseFrontier.py --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True)' 'all:from TriggerJobOpts.TriggerFlags import TriggerFlags as TF;TF.run2Config='\"'\"'2016'\"'\"'' 'RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.cutLevel.set_Value_and_Lock(14); from JetRec import JetRecUtils;f=lambda s:[\"xAOD::JetContainer#AntiKt4%sJets\"%(s,),\"xAOD::JetAuxContainer#AntiKt4%sJetsAux.\"%(s,),\"xAOD::EventShape#Kt4%sEventShape\"%(s,),\"xAOD::EventShapeAuxInfo#Kt4%sEventShapeAux.\"%(s,),\"xAOD::EventShape#Kt4%sOriginEventShape\"%(s,),\"xAOD::EventShapeAuxInfo#Kt4%sOriginEventShapeAux.\"%(s,)]; JetRecUtils.retrieveAODList = lambda : f(\"EMPFlow\")+f(\"LCTopo\")+f(\"EMTopo\")+[\"xAOD::EventShape#NeutralParticleFlowIsoCentralEventShape\",\"xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoCentralEventShapeAux.\", \"xAOD::EventShape#NeutralParticleFlowIsoForwardEventShape\",\"xAOD::EventShapeAuxInfo#NeutralParticleFlowIsoForwardEventShapeAux.\", \"xAOD::EventShape#ParticleFlowIsoCentralEventShape\",\"xAOD::EventShapeAuxInfo#ParticleFlowIsoCentralEventShapeAux.\", \"xAOD::EventShape#ParticleFlowIsoForwardEventShape\",\"xAOD::EventShapeAuxInfo#ParticleFlowIsoForwardEventShapeAux.\", \"xAOD::EventShape#TopoClusterIsoCentralEventShape\",\"xAOD::EventShapeAuxInfo#TopoClusterIsoCentralEventShapeAux.\", \"xAOD::EventShape#TopoClusterIsoForwardEventShape\",\"xAOD::EventShapeAuxInfo#TopoClusterIsoForwardEventShapeAux.\",\"xAOD::CaloClusterContainer#EMOriginTopoClusters\",\"xAOD::ShallowAuxContainer#EMOriginTopoClustersAux.\",\"xAOD::CaloClusterContainer#LCOriginTopoClusters\",\"xAOD::ShallowAuxContainer#LCOriginTopoClustersAux.\"]; from eflowRec.eflowRecFlags import jobproperties; jobproperties.eflowRecFlags.useAODReductionClusterMomentList.set_Value_and_Lock(True); from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODSLIM\");' 'all:from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.btaggingAODList=[\"xAOD::BTaggingContainer#BTagging_AntiKt4EMTopo\",\"xAOD::BTaggingAuxContainer#BTagging_AntiKt4EMTopoAux.\",\"xAOD::BTagVertexContainer#BTagging_AntiKt4EMTopoJFVtx\",\"xAOD::BTagVertexAuxContainer#BTagging_AntiKt4EMTopoJFVtxAux.\",\"xAOD::VertexContainer#BTagging_AntiKt4EMTopoSecVtx\",\"xAOD::VertexAuxContainer#BTagging_AntiKt4EMTopoSecVtxAux.-vxTrackAtVertex\"];' 'ESDtoAOD:from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinGeantTruth.set_Value_and_Lock(True);  AODFlags.ThinNegativeEnergyCaloClusters.set_Value_and_Lock(True); AODFlags.ThinNegativeEnergyNeutralPFOs.set_Value_and_Lock(True); from JetRec import JetRecUtils; aodlist = JetRecUtils.retrieveAODList(); JetRecUtils.retrieveAODList = lambda : [item for item in aodlist if not \"OriginTopoClusters\" in item];' --preInclude HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInclude.PileUpBunchTrainsMC15_2015_25ns_Config1.py,RunDependentSimData/configLumi_run284500_mc16a.py --skipEvents=%SKIPEVENTS --autoConfiguration=everything --conditionsTag default:OFLCOND-MC16-SDR-16 --geometryVersion=default:ATLAS-R2-2016-01-00-01 --runNumber=343237 --digiSeedOffset1=%RNDM:100 --digiSeedOffset2=%RNDM:100 --digiSteeringConf=StandardSignalOnlyTruth --AMITag=r9364 --steering=doRDO_TRIG --inputHighPtMinbiasHitsFile=%IN_MINH --inputLowPtMinbiasHitsFile=%IN_MINL --numberOfCavernBkg=0 --numberOfHighPtMinBias=0.116075313 --numberOfLowPtMinBias=44.3839246425 --pileupFinalBunch=6 --outputAODFile=AOD.pool.root.1 --jobNumber=%RNDM:100 --triggerConfig=RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2136,35,160 "
      opt_args:
        default: "--disableAutoRetry --outputs 'AOD.pool.root.1' --nEventsPerFile=50 --nEventsPerJob 50 --athenaTag Athena,21.0.53 --noBuild --expertOnly_skipScout --avoidVP --secondaryDSs IN_MINH:1:mc16_13TeV.361239.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_high.simul.HITS.e4981_s3087_s3111/,IN_MINL:1:mc16_13TeV.361238.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_low.simul.HITS.e4981_s3087_s3111/ --notExpandSecDSs --forceStaged --forceStagedSecondary"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_deriv:
    run: prun
    in:
      opt_inDS: inner_work_reco/outDS
      opt_exec:
        default: "Reco_tf.py --inputAODFile=%IN --athenaMPMergeTargetSize 'DAOD_*:0' --preExec 'default:rec.doApplyAODFix.set_Value_and_Lock(True); from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-47\"; from AthenaCommon.AlgSequence import AlgSequence; topSequence = AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\"InDetTrackParticlesFixer\", Containers = [ \"InDetTrackParticlesAux.\" ] ); from AthenaMP.AthenaMPFlags import jobproperties as ampjp;ampjp.AthenaMPFlags.UseSharedWriter=True;import AthenaPoolCnvSvc.AthenaPool;ServiceMgr.AthenaPoolCnvSvc.OutputMetadataContainer=\"MetaData\"' --sharedWriter True --runNumber 343237 --AMITag p3870 --passThrough True --outputDAODFile pool.root.1 --reductionConf HIGG2D1"
      opt_args:
        default: "--disableAutoRetry --outputs 'DAOD_HIGG2D1.pool.root.1' --nEventsPerFile=50 --nEventsPerJob 50 --athenaTag AthDerivation,21.2.44.0 --noBuild --expertOnly_skipScout --avoidVP --forceStaged"
      opt_useAthenaPackages:
        default: true
    out: [outDS]


  inner_work_reana:
    run: reana
    in:
      opt_inDS: inner_work_deriv/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-submitter
      opt_exec:
        default: "source script_rucio.sh pchain_ZZ %{DS0} "
      opt_args:
        default: "--site CERN --outputs out-rucio-2.tar --useSecrets"
    out: [outDS] 


  checkpoint:
    run: junction
    in:
      opt_inDS: inner_work_reana/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/steeringcontainer:0.0.7
      opt_exec:
        default: "echo DS0aaaa %{DS0} DS1aaaa %{DS1}; bash script_junction.sh %{i} %{DS0} %{myparamMZD} %{myparamEPSILON}"
      opt_args:
        default: "--site CERN --outputs 'results.json,history.json' --persistentFile history.json --forceStaged"
    out: [outDS]

