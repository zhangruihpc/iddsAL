#!/bin/bash
# ref: https://github.com/reanahub/reana-demo-atlas-recast

job_name=$1
input=$2
echo "this is job name:" $job_name
echo "this is input:" $input
example=reana-demo-atlas-recast
git clone https://github.com/reanahub/${example}.git
cd ${example}

#### create new virtual environment
###virtualenv ~/.virtualenvs/reana
###source ~/.virtualenvs/reana/bin/activate
###source /afs/cern.ch/user/r/reana/public/reana/bin/activate
#### install REANA client
###pip install reana-client
# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN=kUjTYQ-MMe3QJdaakN0jkg
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
reana-client start
## ... should be finished in about a minute
reana-client status
# list workspace files
reana-client ls
## download output results
#reana-client download

# cleanup
cd ../
rm -fr ${example}
