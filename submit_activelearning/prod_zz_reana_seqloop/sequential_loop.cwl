cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs: []

outputs:
  outDS:
    type: string
    outputSource: loop_a_point/outDS

steps:
  loop_a_point:
    run: scatter_body.cwl
    in:
      dataset: []
    out: [outDS]
    hints:
      - loop
