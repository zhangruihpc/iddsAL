#!/usr/bin/env python
# Rui Zhang 03.2023
# rui.zhang@cern.ch

from pandaclient import Client
from argparse import ArgumentParser
import json

def main(args):
    task_id = get_task_id(args.data_set)
    s, output = Client.getUserJobMetadata(task_id)

    result = {}
    result_average = {}
    for data in ["cross-section (nb)", "GenFiltEff", "sumOfPosWeights", "sumOfNegWeights", "sumOfPosWeightsNoFilter", "sumOfNegWeightsNoFilter"]:
        result[data] = []
        for key in output:
            result[data].append(float(output[key]['executor'][0]['metaData'][data]))
    for data in ["cross-section (nb)", "sumOfPosWeights", "sumOfNegWeights", "sumOfPosWeightsNoFilter", "sumOfNegWeightsNoFilter"]:
        result_average[data] = sum(result[data]) / len(result[data])
    result_average["GenFiltEff"] = (sum(result["sumOfPosWeights"]) - sum(result["sumOfNegWeights"])) / (sum(result["sumOfPosWeightsNoFilter"]) - sum(result["sumOfNegWeightsNoFilter"]))
    result_average["cross-section (pb)"] = result_average["cross-section (nb)"] * 1000.
    return {"results": dict(sorted(result.items())), "result_average": dict(sorted(result_average.items()))}

def setup_voms():
    import os
    '''
    You need to setup your voms files and password secrets via pbook in PanDA.
    Check the reference: https://panda-wms.readthedocs.io/en/latest/client/secret.html
    set_secret('VOMS_PASS', '<your voms file password>')
    set_secret("userkey.pem", "<your home dir>/.globus/userkey.pem", is_file=True)
    set_secret("usercert.pem", "<your home dir>/.globus/usercert.pem", is_file=True)
    '''
    command = 'echo '+ os.environ['VOMS_PASS'] +' | voms-proxy-init -cert usercert.pem -key userkey.pem -voms atlas -hours 36 -pwstdin'
    print('setup voms by running:', command)
    os.system(command)

def get_task_id(dataset):
    import subprocess
    cmd = ['rucio', 'list-files', dataset, '--csv']

    # Run the command and capture the output
    print(cmd)
    output = subprocess.check_output(cmd, universal_newlines=True)
    task_id = int(output.split('\n')[0].split(',')[0].split(':')[1].split('.')[2])
    print('Task ID in', dataset, 'is', task_id)
    return task_id

if __name__ == '__main__':

    """Get arguments from command line."""
    parser = ArgumentParser(description="\033[92mExtract cross section and Filter efficiency from generator.log file in EVGEN step.\033[0m")

    parser.add_argument('-d', '--data_set', type=str, default=None, required=False, help='Dataset of the evgen step. Task ID will be extract from the dataset via rucio')
    parser.add_argument('-s', '--skip_voms', action='store_true', required=False, help='Skip voms setup')
    args = parser.parse_args()
    if not args.skip_voms:
        setup_voms()
    results = main(args)
    with open("results.json", "w") as outfile:
        json.dump(results, outfile, indent=2)
    print(results)
    print("Save to results.json")

