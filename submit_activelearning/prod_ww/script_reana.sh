#!/bin/bash
# ref: https://gitlab.cern.ch/zhangruihpc/iddsAL/-/blob/master/submit_activelearning/prod_zz_reana_seqloop/script_reana.sh

loop_counter=$1
sliced_myparamFW=$2
sliced_myparamFWW=$3
sliced_myparamMASS=$4
deriva1=$5
export xsecjson=`\ls $6/*json`
job_name=WW${i}_${sliced_myparamFW}_${sliced_myparamFWW}_${sliced_myparamMASS}

job_name=${job_name//./p}
job_name=${job_name//-/n}
echo "job_name=$job_name"
echo "deriva1=$deriva1"
echo "xsecjson=$xsecjson"
echo "all=${*:2}"

## Update reana.yaml with corresponding dataset
crosssection=`python -c "import json, os; print(json.load(open(os.environ['xsecjson']))['result_average']['cross-section (pb)'])"`
genfilteff=`python -c "import json, os; print(json.load(open(os.environ['xsecjson']))['result_average']['GenFiltEff'])"`

### setup secrets via panda secrets
if [ -z "$REANA_ACCESS_TOKEN" ]; then
    $(cat panda_secrets.json | jq -r 'keys[] as $k | "export \($k)=\(.[$k])"')
fi

example=recast-ww
git clone https://gitlab.cern.ch/zhangruihpc/reana-ww.git ${example}
cd ${example}

\cp ../reana-template.yaml reana.yaml
sed -i "s|VAR_INPUT|$deriva1|g" reana.yaml
sed -i "s|CROSS_SECTION|$crosssection|g" reana.yaml
sed -i "s|GENFILT_EFF|$genfilteff|g" reana.yaml
echo VAR_INPUT = $deriva1
echo CROSS_SECTION = $crosssection
echo GENFILT_EFF = $genfilteff
unset crosssection genfilteff
#####

echo "===== debug ====="
echo cat reana.yaml
cat reana.yaml
echo "===== debug ====="

# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name

# http://docs.reana.io/advanced-usage/access-control/voms-proxy/
echo "setup proxy for both rucio and eos usage; you may only need one according to your demand"
# for rucio usage
reana-client secrets-add --overwrite --file ../userkey.pem --file ../usercert.pem --env VOMSPROXY_PASS=emhhbmdydWkK --env VONAME=atlas
# for eos usage
reana-client secrets-add --overwrite --env CERN_USER=zhangr --env CERN_KEYTAB=keytab --file ../keytab

# delete the pem file from grid site after transferring to reana
echo "delete secrete"
rm -f ../keytab ../usercert.pem ../userkey.pem

echo "upload reana"
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
echo "reana start"
reana-client start
## ... should be finished in about a minute
echo "reana status"
reana-client status --format NAME,RUN_NUMBER,STATUS
echo "get job_num immediately after submission in case other jobs submit a task with the same job_name"
job_num=`reana-client status --format NAME,RUN_NUMBER,STATUS | tr -s ' ' | cut -d " " -f 2 | sed -n 2p`
task_name=${job_name}.${job_num}
echo 'task_name' $task_name
# list workspace files
echo "reana ls"
reana-client ls

cd ../

# wait and download output results # takes about 20min
echo waiting and download output results
status=`reana-client status | python -c $'import sys\nfor line in sys.stdin:print(line.split()[-2])' | sed -n 2p`
echo status: $status
reana-client status
sleep 1800
echo status: $status after 100s
reana-client status
while [[ $status != 'finished' ]]; do
    sleep 120
    status=`reana-client status --format NAME,RUN_NUMBER,STATUS | tr -s ' ' | cut -d " " -f 3 | sed -n 2p`
    echo new status: $status
    if [[ $status == 'failed' ]]; then
        break
    fi
done
echo "reana download"
reana-client download -w ${task_name}
echo {\"loop\": ${loop_counter}, \"myparamMZD\": ${sliced_myparamMZD}, \"myparamEPSILON\": ${sliced_myparamEPSILON}, \"DS\": \"$deriva1\"} > limit_stage/input_param.json
ls fitting ntuplereader
tar -czvf out-rucio-2.tar fitting/ $xsecjson

# cleanup
rm -fr ${example}
