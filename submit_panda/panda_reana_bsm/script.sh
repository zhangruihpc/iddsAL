#!/bin/bash
# ref: https://github.com/reanahub/reana-demo-bsm-search

# source /afs/cern.ch/user/r/reana/public/reana/bin/activate
job_name=$1
input=$2
echo "this is job name:" $job_name
echo "this is input:" $input
example=reana-demo-bsm-search
git clone https://github.com/reanahub/${example}.git
cd ${example}

# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN=kUjTYQ-MMe3QJdaakN0jkg
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name
# upload input code and data to the workspace
echo "reana-client upload ./code"
reana-client upload
echo "reana-client upload ./code done"
# start computational workflow
reana-client start
reana-client ls
##### ... should be finished in about 15 minutes
####reana-client status
##### list output files
####reana-client list | grep ".pdf"
##### download generated plots
####reana-client download plot/postfit.pdf

# cleanup
cd ../
rm -fr ${example}
