#!/usr/bin/env bash
# setup environment
uname -a
alias ls="ls --color=always"

local_dr=$1
export xsecjson=`\ls $2/*json`
echo local_dr=$local_dr
crosssection=`python -c "import json, os; print(json.load(open(os.environ['xsecjson']))['result_average']['cross-section (pb)'])"`
genfilteff=`python -c "import json, os; print(json.load(open(os.environ['xsecjson']))['result_average']['GenFiltEff'])"`

echo xsecjson=$xsecjson crosssection=$crosssection genfilteff=$genfilteff
cat $xsecjson
echo 'pwd'
pwd
echo 'ls'
ls
echo 'ls local_dr'
ls $local_dr
echo 'ls ../'
ls ../

code=/analysis
#code=/afs/cern.ch/user/z/zhangr/work/HPO/WW/cxaodframework_ghh

source /home/atlas/release_setup.sh
source $code/build/*/setup.sh
#cd /analysis/run
#cd /srv
echo before config ls /srv
ls

###################
##### maker  ######
###################

cat $code/build/x86_64-centos7-gcc8-opt/data/CxAODMaker_GHH/framework-run-template.cfg |sed -e "s#99MAXEVENTS99#-1#g" -e "s#99SUBMITDIR99#submitDir#g" -e "s#99INPUTSAMPLE99#${local_dr}#g" > framework-run-local.cfg
echo after config ls /srv
ls
echo cat framework-run-local.cfg
cat framework-run-local.cfg
# no need to overwrite ../build/x86_64-centos7-gcc8-opt/data/CxAODMaker_GHH/framework-run.cfg
echo "echo framework-run-local.cfg done"

# run maker program
echo ls and GHHMaker --submitDir="submitDir" --sampleIn=${local_dr} --numEvents=-1
ls -l ${local_dr}
ls -l ${local_dr}/*
GHHMaker --submitDir="submitDir" --sampleIn=${local_dr} --numEvents=-1

###################
##### reader ######
###################

cat $code/build/x86_64-centos7-gcc8-opt/data/CxAODReader_GHH/framework-read-template.cfg | sed -e "s#99SUBMITDIR99#submitDir_reader#g" -e "s#99DATASETDIR99#submitDir#g" -e "s#99YIELDFILE99#dummy_yields.txt#g" -e '40s/false/true/;42s/^/#/' -e "s#44.3#138.96516#g" -e 's#$WorkDir_DIR/data/CxAODOperations_GHH/XSections_13TeV.txt#XSections_13TeV.txt#g'> framework-read-local.cfg

echo cat framework-read-local.cfg
cat framework-read-local.cfg
echo framework-read-local.cfg done

# need special input folder name, probably hardcoded in CxAODReader*
mv submitDir/data-CxAOD submitDir/CxAOD.root
ln -s $code/source/CxAODOperations_GHH/scripts/count_Nentry_SumOfWeight.py .
#ln -s $code/source/CxAODOperations_GHH/scripts/count_Nentry_SumOfWeight.py ../

echo 'cat count_Nentry_SumOfWeight.py 1'
tail count_Nentry_SumOfWeight.py
#echo 'cat count_Nentry_SumOfWeight.py 2'
#tail ../count_Nentry_SumOfWeight.py
echo 'ls /analysis/CxAODOperations_GHH/scripts/'
ls $code/source/CxAODOperations_GHH/scripts/

# https://bigpanda.cern.ch//media/filebrowser/3ba7dab4-95dc-4061-b838-67a265205e75/user.zhangr/tarball_PandaJob_5777149285_GOOGLE_EUW1_DYN/log.generate
# search for "MetaData: cross-section (nb)" [x1000 as the second number below]; "GenFiltEff" [fourth number below]
echo "999999      ${crosssection}          1.0        ${genfilteff}     a   b" >> XSections_13TeV.txt ### dummy input; need to insert cross section to ../build/x86_64-centos7-gcc8-opt/data/CxAODOperations_GHH/XSections_13TeV.txt with format "dsid Xsec[pb] kfactor(1) filtereff(~0.1)   dummy dummy"
echo "tail XSections_13TeV.txt"
tail XSections_13TeV.txt

# run reader program
echo ls 2 GHHReader --config framework-read-local.cfg
WorkDir_DIR_orig=$WorkDir_DIR
WorkDir_DIR='./'
mkdir -p ${WorkDir_DIR}/data/CxAODTools/ ${WorkDir_DIR}/data/CxAODReader_GHH/FakeFactor/ ${WorkDir_DIR}/data/CxAODReader_GHH/ChargeFlipRate/
\cp $code/source/CxAODTools/data/PtReco_histos_2_qqZllHbb_345055_mc16ade_all_OneMu_TruthWZ_CorrectionFactor.root ${WorkDir_DIR}/data/CxAODTools/
\cp $code/build/x86_64-centos7-gcc8-opt/data/CxAODReader_GHH/FakeFactor/ff_ssWWPAOD_2D_Nominal.root ${WorkDir_DIR}/data/CxAODReader_GHH/FakeFactor/
\cp $code/build/x86_64-centos7-gcc8-opt/data/CxAODReader_GHH/ChargeFlipRate/*.root ${WorkDir_DIR}/data/CxAODReader_GHH/ChargeFlipRate/
ls -lht
ls -lht *
ls -lht */*
echo "ls submitDir"
ls submitDir
echo "============= GHHReader "
GHHReader --config framework-read-local.cfg
WorkDir_DIR=$WorkDir_DIR_orig

mv submitDir_reader/data-MVATree/CxAOD.root.root ActLearn.root
