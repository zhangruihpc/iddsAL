#!/bin/bash
# ref: https://github.com/reanahub/reana-demo-atlas-recast

source /afs/cern.ch/user/r/reana/public/reana/bin/activate
job_name=locWW01
#input=$2
echo "this is job name:" $job_name
echo "this is input:" $input
example=reana-ww
#example=reana-zz
#example=ana-exot-2018-046-eleni
#example=reana-my-recast-rucio
#example=reana-my-recast-eos
#rm -fr $example
#git clone https://gitlab.cern.ch/zhangruihpc/reana-ana-exot-2018-046.git ${example}
#git clone ssh://git@gitlab.cern.ch:7999/recast-atlas/exotics/ana-exot-2018-046.git ${example}
cd ${example}
echo "===== debug ====="
ls *
ls ../
echo "===== debug ====="

#### create new virtual environment
###virtualenv ~/.virtualenvs/reana
###source ~/.virtualenvs/reana/bin/activate
###source /afs/cern.ch/user/r/reana/public/reana/bin/activate
#### install REANA client
###pip install reana-client
# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
export REANA_ACCESS_TOKEN=kUjTYQ-MMe3QJdaakN0jkg
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name

# http://docs.reana.io/advanced-usage/access-control/voms-proxy/
echo "setup proxy"
reana-client secrets-add --overwrite --file ../userkey.pem --file ../usercert.pem --env VOMSPROXY_PASS=emhhbmdydWkK --env VONAME=atlas
reana-client secrets-add --overwrite --env CERN_USER=zhangr --env CERN_KEYTAB=keytab --file ../keytab

echo "upload reana"
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
reana-client start
## ... should be finished in about a minute
reana-client status --format NAME,RUN_NUMBER,STATUS
job_num=`reana-client status --format NAME,RUN_NUMBER,STATUS | tr -s ' ' | cut -d " " -f 2 | sed -n 2p`
task_name=${job_name}.${job_num}
# list workspace files
reana-client ls

cd ../
while [[ $status != 'finished' ]]; do
    sleep 30
    status=`reana-client status --format NAME,RUN_NUMBER,STATUS | tr -s ' ' | cut -d " " -f 3 | sed -n 2p`
done

# download output results
reana-client download -w ${task_name}

# cleanup
#cd ../
#rm -fr ${example}
