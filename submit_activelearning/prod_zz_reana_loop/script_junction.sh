#!/bin/bash
# ref: https://gitlab.cern.ch/recast-atlas/exotics/ana-exot-2018-046/-/tree/reana_working

echo "=== ls folder begin ==="
ls -ld *; 
echo "=== ls . begin ==="
ls -l *; 
echo "=== pwd begin ==="
pwd; 

#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh 
#echo "setupATLAS"
#setupATLAS
#echo "lsetup rucio"
#lsetup rucio
#echo "localSetup RucioClient"
#localSetupRucioClients
#echo "voms-proxy-info"
#voms-proxy-info
#echo "rucio -v get" $2
##rucio -v get $2
#rucio get $2
#echo setupATLAS done

echo "=== ls folder ==="
ls -ld *; 
echo "=== ls . ==="
ls -l *; 
echo "=== pwd ==="
pwd; 

echo "cat"
cat results.json; 
#echo 'untar'; 
#for i in `\\ls -d *tar`; 
#    do tar xvf $i/*tar -C $i; 
#    echo "check tar" $i
#    ls $i
#done; 
tar xvf *tar

ls -l ; 
echo 'ls out-rucio-2.tar'; 
ls -l *; 
echo 'ls limit_stage'; 
ls -l */*; 
echo 'run hpogrid'; 
if [[ $1 -ge 4 ]]; then 
    to_continue='false'
else
    to_continue='true'
fi

echo python active_learning_zz.py -s parameter_space.json -l limit_stage/output.json -pv myparamMZD $3 myparamEPSILON $4 -c $to_continue;
python active_learning_zz.py -s parameter_space.json -l limit_stage/output.json -pv myparamMZD $3 myparamEPSILON $4 -c $to_continue;
ls -l;
echo 'cat history';
cat history.json;
echo 'cat results.json';
cat results.json;
