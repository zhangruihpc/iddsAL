cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}


inputs:
  seeds: int[]

outputs:
  outDS:
    type: string
    outputSource: recast/outDS


steps:
  prod:
    run: evnt_to_deriv_large.cwl
    #run: evnt_to_deriv.cwl
    scatter: [seed]
    scatterMethod: dotproduct
    in:
      seed: seeds
    out: [outDS]

  recast:
    run: reana
    in:
      opt_inDS: prod/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-submitter
      opt_exec:
        default: "source script_rucio.sh pchain_ZZ %{DS0} "
      opt_args:
        default: "--disableAutoRetry --site  CERN --outputs out-rucio-2.tar --useSecrets"
    out: [outDS]
