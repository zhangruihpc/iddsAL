cwlVersion: v1.0
class: Workflow

requirements:
  MultipleInputFeatureRequirement: {}

inputs:
  param_sliced_myparamFW:
    type: float
  param_sliced_myparamFWW:
    type: float
  param_sliced_myparamMASS:
    type: float
  BKGH: string

outputs:
  outDS:
    type: string
    outputSource: step_FIT/outDS


steps:
  step_evgen:
    run: prun
    in:
      opt_exec:
        default: "echo sliced_myparamFW = %{sliced_myparamFW}; echo param_sliced_myparamFWW = %{sliced_myparamFWW}; export JOBOPTSEARCHPATH=$PWD:$JOBOPTSEARCHPATH; export envPARAM_FW=%{sliced_myparamFW}; export envPARAM_FWW=%{sliced_myparamFWW}; export envPARAM_MASS=%{sliced_myparamMASS}; echo $envPARAM_FWW; Gen_tf.py --maxEvents=%MAXEVENTS --skipEvents=0 --ecmEnergy=13000 --firstEvent=%FIRSTEVENT:0 --outputEVNTFile=EVNT.pool.root.1 --randomSeed=%RNDM:100 --runNumber=999999 --AMITag=e8273 --jobConfig=999999; unset envPARAM_FW envPARAM_FWW envPARAM_MASS"
      opt_args:
        default: "--excludedSite IN2P3-LPC --outputs 'EVNT.pool.root.1' -y --memory=5000 --nEvents=50000 --nJobs 100 --athenaTag AthGeneration,21.6.92 --noBuild --expertOnly_skipScout --avoidVP"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  step_extXS:
    run: prun
    in:
      opt_inDS: step_evgen/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-voms
      opt_exec:
        default: "echo DS1=%{DS1}; echo IN=%{IN}; pwd; ls -l; echo /home/panda/; ls /home/panda/; python extract_xsec.py -d %{DS1}"
      opt_args:
        default: "--site CERN --useSecrets --outputs 'results.json' --avoidVP"
    out: [outDS]

  step_simul:
    run: prun
    in:
      opt_inDS: step_evgen/outDS
      opt_exec:
        default: "Sim_tf.py --inputEVNTFile=%IN --maxEvents=%MAXEVENTS --postInclude default:RecJobTransforms/UseFrontier.py --preExec 'EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)' 'EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True' --preInclude EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py --skipEvents=%SKIPEVENTS --firstEvent=%FIRSTEVENT:0 --outputHITSFile=HITS.pool.root.1 --physicsList=FTFP_BERT_ATL_VALIDATION --randomSeed=%RNDM:1 --DBRelease=all:current --conditionsTag default:OFLCOND-MC16-SDR-14 --geometryVersion=default:ATLAS-R2-2016-01-00-01_VALIDATION --runNumber=999999 --AMITag=s3126 --DataRunNumber=284500 --simulator=FullG4 --truthStrategy=MC15aPlus"
      opt_args:
        default: "--excludedSite IN2P3-LPC --outputs 'HITS.pool.root.1' --nEventsPerFile=500 --nEventsPerJob 50 --athenaTag Athena,21.0.31 --noBuild --expertOnly_skipScout --avoidVP --forceStaged"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  step_recon:
    run: prun
    in:
      opt_inDS: step_simul/outDS
      opt_exec:
        default: "Reco_tf.py --inputHITSFile=%IN --maxEvents=%MAXEVENTS --postExec 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"]' 'ESDtoAOD:fixedAttrib=[s if \"CONTAINER_SPLITLEVEL = '\"'\"'99'\"'\"'\" not in s else \"\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' --postInclude default:PyJobTransforms/UseFrontier.py --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODSLIM\");' --preInclude HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_run310000.py --skipEvents=%SKIPEVENTS --autoConfiguration=everything --conditionsTag default:OFLCOND-MC16-SDR-20 --geometryVersion=default:ATLAS-R2-2016-01-00-01 --runNumber=999999 --digiSeedOffset1=%RNDM:1 --digiSeedOffset2=%RNDM:1 --digiSteeringConf=StandardSignalOnlyTruth --AMITag=r10724 --steering=doRDO_TRIG --inputHighPtMinbiasHitsFile=%IN_MINH --inputLowPtMinbiasHitsFile=%IN_MINL --numberOfCavernBkg=0 --numberOfHighPtMinBias=0.2595392 --numberOfLowPtMinBias=99.2404608 --pileupFinalBunch=6 --outputAODFile=AOD.pool.root.1 --jobNumber=%RNDM:1 --triggerConfig=RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2216,76,260 --valid=True"
      opt_args:
        default: "--excludedSite IN2P3-LPC --outputs 'AOD.pool.root.1' --nEventsPerFile=50 --nEventsPerJob 500 --athenaTag Athena,21.0.77 --noBuild --expertOnly_skipScout --avoidVP --secondaryDSs IN_MINH:1:mc16_13TeV.361239.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_high.simul.HITS.e4981_s3087_s3111/,IN_MINL:1:mc16_13TeV.361238.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_low.simul.HITS.e4981_s3087_s3111/ --notExpandSecDSs --forceStaged --forceStagedSecondary"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  step_deriv:
    run: prun
    in:
      opt_inDS: step_recon/outDS
      opt_exec:
        default: "Reco_tf.py --inputAODFile=%IN --athenaMPMergeTargetSize 'DAOD_*:0' --preExec 'default:from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-49\"; from AthenaCommon.AlgSequence import AlgSequence; topSequence = AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\"InDetTrackParticlesFixer\", Containers = [ \"InDetTrackParticlesAux.\" ] );from AthenaMP.AthenaMPFlags import jobproperties as ampjp;ampjp.AthenaMPFlags.UseSharedWriter=True;import AthenaPoolCnvSvc.AthenaPool;ServiceMgr.AthenaPoolCnvSvc.OutputMetadataContainer=\"MetaData\";topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\"BTaggingELFixer\", Containers = [\"BTagging_AntiKt4EMTopoAux.\" ] );' --sharedWriter True --runNumber 999999 --AMITag p4310 --outputDAODFile pool.root.1 --reductionConf HIGG2D4 --passThrough True"
      opt_args:
        default: "--excludedSite IN2P3-LPC --outputs 'DAOD_HIGG2D4.pool.root.1' --nEventsPerFile=500 --nEventsPerJob 500 --athenaTag AthDerivation,21.2.108.0 --noBuild --expertOnly_skipScout --avoidVP --forceStaged"
      opt_useAthenaPackages:
        default: true
    out: [outDS]


  step_CxAOD:
    run: prun
    in:
      opt_inDS: step_deriv/outDS
      opt_secondaryDSs: [step_extXS/outDS]
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/atlas-ghh-analyses/cxaodframework_ghh:disable_prw_for_activelearning-ca4de372
      opt_exec:
        default: "mkdir input_tmp/; mv user*root* input_tmp/; mv input_tmp ../%{DS1}; mkdir json_tmp/; mv %IN2 json_tmp/; mv json_tmp ../%{SECDS1}; echo ls; ls -l; echo ls ../; ls -l ../; echo ls ../xx; ls -l ../*; echo DS1=%{DS1}; echo SECDS1=%{SECDS1}; source CxAOD.sh ../%{DS1} ../%{SECDS1}"
      opt_args:
        default: "--useSecrets --outputs 'ActLearn.root' --avoidVP --forceStaged --forceStagedSecondary --secondaryDSs IN2:2:%{SECDS1}"
    out: [outDS]

  step_NTUP:
    run: prun
    in:
      opt_inDS: step_CxAOD/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/atlas-ghh-analyses/ntuplereader_ghh:disable_prw_for_activelearning-a44771ad
      opt_exec:
        default: "echo pwd; pwd; echo ls -l; ls -l; echo ls -l ..; ls -l ..; echo ls -l ../..; ls -l ../..; mkdir input_tmp/; mv user*root* input_tmp/; mv input_tmp ../%{DS1}; echo ls -l ../*; ls -l ../*; echo DS1=%{DS1}; source Ntuple.sh ../%{DS1}"
      opt_args:
        default: "--useSecrets --outputs 'output_2Lep_ActLearn.root' --avoidVP --forceStaged"
    out: [outDS]

  step_FIT:
    run: prun
    in:
      opt_inDS: step_NTUP/outDS
      opt_secondaryDSs: [BKGH, step_extXS/outDS]
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/atlas-ghh-analyses/fittingcode:ForRecast-acbf4dc3
      opt_exec:
        default: "echo pwd; pwd; echo ls -l; ls -l; echo ls -l ..; ls -l ..; echo ls -l ../..; ls -l ../..; mkdir input_tmp/; mv user*ActLearn.root input_tmp/; mv input_tmp ../%{DS1}; mkdir input_tmp/; mv %IN2 input_tmp/; mv input_tmp ../%{SECDS1}; mkdir input_tmp/; cp %IN3 input_tmp/; mv input_tmp ../%{SECDS2}; echo after ls -l ../*; ls -l ../*; echo DS1=%{DS1}  SECDS1=%{SECDS1} SECDS2=%{SECDS2}; source Fit.sh ../%{DS1} ../%{SECDS1}; echo FW, FWW, MASS > myworkdir/point.csv; echo %{sliced_myparamFW}, %{sliced_myparamFWW}, %{sliced_myparamMASS} >> myworkdir/point.csv; echo %{SECDS1}, %{SECDS2}, %{DS1} >> myworkdir/point.csv; cat myworkdir/point.csv; echo ====; cp -r ../%{SECDS2} myworkdir; echo final ls myworkdir; ls myworkdir; ls myworkdir/*; tar cvf myworkdir.tar myworkdir"
      opt_args:
        default: "--maxAttempt 1 --site CERN --useSecrets --outputs 'myworkdir.tar' --avoidVP --forceStaged --forceStagedSecondary --secondaryDSs IN2:5:%{SECDS1},IN3:5:%{SECDS2}"
    out: [outDS]

