cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  BKGH: string

outputs:
  outDS:
    type: string
    outputSource: loop_a_point/outDS

steps:
  loop_a_point:
    run: scatter_body.cwl
    in:
      BKGH: BKGH
    out: [outDS]
    hints:
      - loop
