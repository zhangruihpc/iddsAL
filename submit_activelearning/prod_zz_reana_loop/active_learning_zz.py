import json
from pdb import set_trace
from argparse import ArgumentParser
from os import environ, makedirs, path
from hpogrid.core.defaults import GeneratorMethod
from hpogrid.interface.idds.steering import SteeringIDDS

# python module/active_learning_zz.py -pv myparamMZD 30 myparamMH 60
def main(args):
    with open(args.input_loss) as fp:
        loss = json.load(fp)['loss']

    result = {
        'param': {},
        'loss': loss,
    }
    for p, v in zip(args.param_values_pair[::2], args.param_values_pair[1::2]):
        result['param'][p] = float(v)

    with open(args.parameter_space) as fp:
        space = json.load(fp)

    SteeringIDDS().run_generator(args.parameter_space, mode='max', n_point=args.n_point, lib=GeneratorMethod.SKOPT, outfile=args.outfile)
    # will generate following format (n_point=1)
    # [
    #     {
    #         "myparamMZD": 0.007085601078096873,
    #         "myparamMH": 0.722876619143738
    #     }
    # ]
    with open(args.outfile) as fp:
        new_points = json.load(fp)[0]
    with open(args.outfile, 'w') as fp:
        new_points['to_continue'] = True if args.to_continue.lower() == 'true' else False
        json.dump(new_points, fp, indent=2)

    if path.exists(args.history) and path.getsize(args.history) > 0:
        with open(args.history) as fp:
            history = json.load(fp)
        history['points'].append([result['param'], loss])
        with open(args.history, 'w') as fp:
            json.dump(history, fp, indent=2)
    else:
        history = {
            'points': [],
            'opt_space': space,
        }
        history['points'].append([result['param'], loss])
        with open(args.history, 'w') as fp:
            json.dump(history, fp, indent=2)

    return result

if __name__ == '__main__':

    """Get arguments from command line."""
    parser = ArgumentParser(description="Workforce to optimise domain space")
    parser.add_argument('-l', '--input-loss', default = 'limit_stage/output.json', type = str, help = 'Json file for "loss" value produced by ZX_PostProcessingPlotter')
    parser.add_argument('-s', '--parameter-space', default = 'parameter_space.json', type = str, help='Search space')
    parser.add_argument('-n', '--n-point', default = 1, type = int, help='Number of new points to generate')
    parser.add_argument('-pv', '--param-values-pair', nargs='+', required=True, help = 'Parameter value pairs from this loop')
    parser.add_argument('-o', '--outfile', default = 'results.json', type = str, help='new point')
    parser.add_argument('-a', '--history', default = 'history.json', type = str, help='all results')
    parser.add_argument('-c', '--to_continue', default = 'true', type = str, help='To continue')

    main(parser.parse_args())
