version=v01
for name in mc15_13TeV.123456.cap_recast_demo_signal_one.root mc15_13TeV.789012.cap_recast_demo_signal_two.root ; do
    rucio upload --rse BNL-OSG2_SCRATCHDISK /afs/cern.ch/user/z/zhangr/wis/HPO/Reana/demo_input/${name} --name ${name}
    if [[ -v RUCIO_ACCOUNT ]]; then
        rucio add-dataset user.${RUCIO_ACCOUNT}.reana_demo.${version}.dataset
        rucio attach user.${RUCIO_ACCOUNT}:user.${RUCIO_ACCOUNT}.reana_demo.${version}.dataset user.zhangr:${name}
    fi
done
unset version name
