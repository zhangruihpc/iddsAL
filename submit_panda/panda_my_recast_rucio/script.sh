#!/bin/bash
# ref: https://github.com/reanahub/reana-demo-atlas-recast

job_name=$1
input=$2
echo "this is job name:" $job_name
echo "this is input:" $input

## setup secrets via panda secrets
$(cat panda_secrets.json | jq -r 'keys[] as $k | "export \($k)=\(.[$k])"')
cat panda_secrets.json
echo $REANA_ACCESS_TOKEN

example=reana-my-recast-rucio
git clone https://gitlab.cern.ch/zhangruiForked/reana-demo-atlas-recast-2.git ${example}
cd ${example}
echo "===== debug ====="
ls *
ls ../
echo "===== debug ====="

# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name

# http://docs.reana.io/advanced-usage/access-control/voms-proxy/
echo "setup proxy"
reana-client secrets-add --overwrite --file ../userkey.pem --file ../usercert.pem --env VOMSPROXY_PASS=emhhbmdydWkK --env VONAME=atlas

# delete the pem file from grid site after transferring to reana
echo "delete secrete"
rm -f ../keytab ../usercert.pem ../userkey.pem

echo "upload reana"
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
reana-client start
## ... should be finished in about a minute
reana-client status
# list workspace files
reana-client ls

cd ../

# wait and download output results
echo status: $status
while [[ $status != 'finished' ]]; do
    sleep 30
    status=`reana-client status | tr -s ' ' | cut -d " " -f 6 | sed -n 2p`
    echo new status: $status
    if [[ $status == 'failed' ]]; then
        break
    fi
done
reana-client download
tar -czvf statanalysis.tar statanalysis

echo ----- debug ----
pwd
ls
echo ----- debug ----

# cleanup
rm -fr ${example}
