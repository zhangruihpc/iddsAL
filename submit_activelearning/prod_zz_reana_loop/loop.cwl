cwlVersion: v1.0
class: Workflow

requirements:
  SubworkflowFeatureRequirement: {}

inputs: []

outputs:
  outDS:
    type: string
    outputSource: work_loop/outDS


steps:
  work_start:
    run: prun
    in:
      opt_exec:
        default: "echo %RNDM:10 > input.txt"
      opt_args:
        default: "--outputs input.txt --site CERN --nJobs 1 --avoidVP"
    out: [outDS]

  work_loop:
    run: loop_body.prod.cwl
    #run: loop_body.prod_large.cwl
    #run: loop_body.official_large.cwl
    #run: loop_body.official_small.cwl
    in:
      dataset: work_start/outDS
    out: [outDS]
    hints:
      - loop
  
