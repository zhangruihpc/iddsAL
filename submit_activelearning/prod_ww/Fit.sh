#!/usr/bin/env bash
# setup environment
uname -a
alias ls="ls --color=always"

input_file=`\ls $1/*.root`
bkg_file=`\ls $2/*.root`
echo input_file=$input_file
echo bkg_file=$bkg_file

source /home/atlas/release_setup.sh
which HistFitter.py
#code=/afs/cern.ch/user/z/zhangr/work/HPO/WW/FittingCode
code=/fitting
#cd /fitting && mkdir -p run
#cd run

echo "start rebinning"
root -b -q $code/scripts/prepareInput/Rebin_GHH.cc"(\"${input_file}\", \"ActLearn_step1\")"

echo "prepare the input for fitter"
ln -s $code/scripts/prepareInput/ForFitter.cc .
ln -s $code/scripts/prepareInput/include.h .
root -b -q ForFitter.cc"(\"ActLearn_step1\", \"ActLearn_step2\")"

hadd backupCacheFile_GHH_ss2l_sys_slim.root ActLearn_step2.root ${bkg_file}

ls
rm -rf ${bkg_file}
cp -r $code/HistFitter/config .
echo 'ls *'
ls *

# generate workspace
HistFitter.py -w -F excl -c "mode='excl_Inclusive_sys'; suffix='ActLearn';" $code/systematics/GHH_ActLearn.py
ls -l results/GHH_excl_Inclusive_sys_ActLearn/Exclusion_combined_NormalMeasurement_model.root
quickstats cls_limit -i results/GHH_excl_Inclusive_sys_ActLearn/Exclusion_combined_NormalMeasurement_model.root -d obsData --unblind -p mu_SIG

myworkdir=myworkdir
mkdir -p ${myworkdir}
mv ./results/GHH_excl_Inclusive_sys_ActLearn/Exclusion_combined_NormalMeasurement_model.root ${myworkdir}/workspace_sys.root
mv ActLearn_step2.root ${myworkdir}/
cp limits*json ${myworkdir}/
echo "==="
echo ls ${myworkdir}
ls ${myworkdir}
echo "==="
#tar cvf ${myworkdir}.tar ${myworkdir}
#echo ls again ${myworkdir}
#ls ${myworkdir}
