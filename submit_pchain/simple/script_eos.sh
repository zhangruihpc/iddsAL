#!/bin/bash
# ref: https://github.com/reanahub/reana-demo-atlas-recast

job_name=$1
input=$2

### setup secrets via panda secrets
if [ -z "$REANA_ACCESS_TOKEN" ]; then
    $(cat panda_secrets.json | jq -r 'keys[] as $k | "export \($k)=\(.[$k])"')
fi

example=reana-my-recast-eos
git clone https://gitlab.cern.ch/zhangruihpc/reana-demo-atlas-recast.git ${example}
cd ${example}
echo "===== debug ====="
ls *
ls ../
echo "===== debug ====="

# connect to some REANA cloud instance
export REANA_SERVER_URL=https://reana.cern.ch/
# create new workflow
reana-client create -w $job_name
export REANA_WORKON=$job_name

# http://docs.reana.io/advanced-usage/access-control/voms-proxy/
echo "setup proxy"
#reana-client secrets-add --overwrite --file ../userkey.pem --file ../usercert.pem --env VOMSPROXY_PASS=emhhbmdydWkK --env VONAME=atlas
reana-client secrets-add --overwrite --env CERN_USER=zhangr --env CERN_KEYTAB=keytab --file ../keytab

# delete the pem file from grid site after transferring to reana
echo "delete secrete"
rm -f ../keytab ../usercert.pem ../userkey.pem

echo "upload reana"
# upload input code, data and workflow to the workspace
reana-client upload
# start computational workflow
echo "reana start"
reana-client start
## ... should be finished in about a minute
echo "reana status"
reana-client status
# list workspace files
echo "reana ls"
reana-client ls

cd ../

# wait and download output results
echo waiting and download output results
status=`reana-client status | python -c $'import sys\nfor line in sys.stdin:print(line.split()[-2])' | sed -n 2p`
echo status: $status
reana-client status
while [[ $status != 'finished' ]]; do
    sleep 30
    status=`reana-client status | python -c $'import sys\nfor line in sys.stdin:print(line.split()[-2])' | sed -n 2p`
    echo new status: $status
    if [[ $status == 'failed' ]]; then
        break
    fi
done
echo "reana download"
reana-client download
tar -czvf out-eos-1.tar statanalysis

echo ----- debug ----
pwd
ls
echo ----- debug ----

# cleanup
rm -fr ${example}
