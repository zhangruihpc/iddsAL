cwlVersion: v1.0
class: Workflow

inputs:
  param_sliced_myparamMZD:
    type: float
  param_sliced_myparamEPSILON:
    type: float

outputs:
  outDS:
    type: string
    outputSource: inner_work_reana/outDS


steps:
  inner_work_evgen:
    run: prun
    in:
      opt_exec:
        default: "export JOBOPTSEARCHPATH=$PWD:$JOBOPTSEARCHPATH; echo 'before' %{i} %{myparamEPSILO} %{myparamMZD}; export envPARAM_MHD=%{myparamMHD}; export envPARAM_MZD=%{myparamMZD}; echo 'check env2'; python -c 'import os; print(os.environ[\"envPARAM_MZD\"]); print(os.environ[\"envPARAM_MHD\"])'; echo something > EVNT.pool.root.1; cat EVNT.pool.root.1"
      opt_args:
        default: "--forceStaged --noBuild --avoidVP --disableAutoRetry --site CERN --outputs 'EVNT.pool.root.1'"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_simul:
    run: prun
    in:
      opt_inDS: inner_work_evgen/outDS
      opt_exec:
        default: "echo something > HITS.pool.root.1; echo HITS.pool.root.1"
      opt_args:
        default: "--forceStaged --noBuild --avoidVP --disableAutoRetry --site CERN --outputs 'HITS.pool.root.1'"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_reco:
    run: prun
    in:
      opt_inDS: inner_work_simul/outDS
      opt_exec:
        default: "echo something > AOD.pool.root.1; echo AOD.pool.root.1"
      opt_args:
        default: "--forceStaged --noBuild --avoidVP --disableAutoRetry --site CERN --outputs 'AOD.pool.root.1' "
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_deriv:
    run: prun
    in:
      opt_inDS: inner_work_reco/outDS
      opt_exec:
        default: "echo something> DAOD_HIGG2D1.pool.root.1; echo DAOD_HIGG2D1.pool.root.1"
      opt_args:
        default: "--forceStaged --noBuild --avoidVP --disableAutoRetry --site CERN --outputs 'DAOD_HIGG2D1.pool.root.1' "
      opt_useAthenaPackages:
        default: true
    out: [outDS]


  inner_work_reana:
    run: reana
    in:
      opt_inDS: inner_work_deriv/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-submitter
      opt_exec:
        default: "echo DS0 = %{DS0}; source script_reana.sh pchain_ZZ %{DS0} "
      opt_args:
        default: "--noBuild --avoidVP --disableAutoRetry --site CERN --outputs out-rucio-2.tar --useSecrets"
    out: [outDS] 
