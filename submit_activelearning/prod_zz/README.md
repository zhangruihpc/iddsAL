Christian Weber provided these examples:

## evnt_to_deriv2.cwl:
DSID=512443
- evgen: https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/43259/
- sim+reco: https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/43278/
- derivation (another example but HIGG2D1 is what we want for DSID=343234): https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/43424/

Step:       Name in cwl:    Format:                         Transformer
0:          evnt:                -> EVNT:                   Gen_tf.py
1:          ---:            EVNT -> EVNT:                   EVNTMerge_tf.py
2:          simul:          EVNT -> HIST:                   Sim_tf.py
3:          reco:           HIST -> AOD:                    Reco_tf.py
4:          ---:            AOD  -> AOD:                    AODMerge_tf.py
5:          ntup_pile:      AOD  -> NTUP_PILEUP:            PRWConfig_tf.py
6:          ---:            NTUP_PILEUP -> NTUPLE_PILEUP:   NTupMerge.py

## evnt_to_deriv3.cwl:
DSID=343234
- evgen: https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/19959/
- sim+reco: https://prodtask-dev.cern.ch/prodtask/inputlist_with_request/19957/

## evnt_to_deriv4.cwl:
DSID=999999 (customised)
Same as 343234 but only change the runNumber
