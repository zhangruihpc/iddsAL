import json
import pandas as pd
from pdb import set_trace
from argparse import ArgumentParser
from os import environ, makedirs, path, system
from glob import glob
from hpogrid.interface.idds.steering import SteeringIDDS

def collect_limits():
    all_limits = []
    outputs = glob('reana_output/*/limit_stage/')
    for o in outputs:
        param = f'{o}/input_param.json'
        fp = open(param, 'r')
        param = json.load(fp)
        limits = f'{o}/limits.csv'
        limits = pd.read_csv(limits)
        for i in ['myparamMZD', 'myparamEPSILON']:
            limits[i] = param[i]
        all_limits.append(limits)
    
    df = pd.concat(all_limits).sort_values(by=['myparamMZD'])
    df.to_csv('allresults.csv')
    print('Save all limits to', 'allresults.csv')

def extract_reana_results1(all_param_json, save=False):

    with open(all_param_json, 'r') as f:
        parameters = json.load(f)

    all_tar = glob('*.tar')
    results = {
        'param': parameters,
        'loss': [],
    }
    for i in range(len(all_tar)):
        extract_folder = f'reana_output/{i}'
        makedirs(extract_folder, exist_ok=True)
        system(f'tar xf {all_tar[i]} -C {extract_folder}')
        results['loss'].append(json.load(open(f'{extract_folder}/limit_stage/output.json'))['loss'])

    if save:
        with open('reana_results.json', 'w') as f:
            json.dump(results, f, indent=2)
    print('method1', results)
    return results

def extract_reana_results2(args):

    all_tar = glob('*.tar')
    results = {
        'param': {},
        'loss': [],
    }
    for par in args.params:
        results['param'][par] = []
    for i in range(len(all_tar)):
        extract_folder = f'reana_output/{i}'
        makedirs(extract_folder, exist_ok=True)
        system(f'tar xf {all_tar[i]} -C {extract_folder}')
        results['loss'].append(json.load(open(f'{extract_folder}/limit_stage/output.json'))['loss'])
        param = json.load(open(f'{extract_folder}/limit_stage/input_param.json'))
        for par in args.params:
            results['param'][par].append(param[par])

    print('method2', results)
    return results

def convert_to_points(result):
    '''
    result = {
        "param": {
            "p1": [],
            "p2": [],
        },
        "loss": []
    }

    Below format is used for hpogrid
    {
        "points": [
            [
                {
                    "p1": p1_value,
                    "p2": p1_value,
                },
                loss_value
            ],
            [
                {
                    "p1": p1_value2,
                    "p2": p1_value2,
                },
                loss_value2
            ],
        ]
    }
    '''
    format_result = {
        "points": [],
    }
    result_flatten = result["param"]
    param_names = result_flatten.keys()

    for values in zip(*([result_flatten[p] for p in param_names] + [result["loss"]])):
        point = dict(zip(param_names, values[:-1]))
        loss = values[-1]
        one_point = [point, loss]
        format_result["points"].append(one_point)
    return format_result

def convert_to_list(result, to_continue):
    '''
    result = [
        {
            "p1": p1_value1,
            "p2": p2_value1,
        },
        {
            "p1": p1_value2,
            "p2": p2_value2,
        },
    ]

    convert to:
    {
        "p1": [p1_value1, p1_value2],
        "p2": [p2_value1, p2_value2],
        "to_continue": to_continue
    }
    '''
    format_result = {}
    for k in result[0].keys():
        format_result[k] = [d[k] for d in result]
    format_result["to_continue"] = to_continue
    return format_result

def main(args):

    if path.exists(args.all_param_json):
        reana_result = extract_reana_results1(args.all_param_json, True)
    else:
        reana_result = extract_reana_results2(args)

    # Update/Generate history.json from current jobs
    result_for_history = convert_to_points(reana_result)
    if path.exists(args.history) and path.getsize(args.history) > 0:
        with open(args.history) as fp:
            history = json.load(fp)

        # iDDS will place all *out-rucio-2.tar in the current workarea, not only these from the current loop
        # Therefore no need to `extend`
        #history['points'].extend(result_for_history['points'])
        history['points'] = result_for_history['points']

        with open(args.history, 'w') as fp:
            json.dump(history, fp, indent=2)
    else:
        with open(args.parameter_space) as fp:
            space = json.load(fp)
            result_for_history['opt_space'] = space
        with open(args.history, 'w') as fp:
            json.dump(result_for_history, fp, indent=2)

    # Load the results in args.history, generate n_points, save them to args.outfile
    SteeringIDDS().run_generator(args.parameter_space, mode='max', n_point=args.n_point, max_point=int(1E6), lib="skopt", infile=args.history, outfile=args.outfile)

    # Convert iDDS output to pchain output
    with open(args.outfile) as fp:
        new_points = json.load(fp)
        new_points = convert_to_list(new_points, int(args.current_round) < int(args.max_rounds)) 
    with open(args.outfile, 'w') as fp:
        new_points['to_continue'] = True if int(args.current_round) < int(args.max_rounds) else False
        json.dump(new_points, fp, indent=2)

    # Inspect contents of json files
    system(f'echo cat {args.outfile}')
    system(f'cat {args.outfile}')
    system(f'echo cat {args.history}')
    system(f'cat {args.history}')
    collect_limits()
    return

if __name__ == '__main__':

    """Get arguments from command line."""
    parser = ArgumentParser(description="Workforce to optimise domain space")
    parser.add_argument('-s', '--parameter_space', default = 'parameter_space.json', type = str, help='Search space')
    parser.add_argument('-i', '--all_param_json', default = 'all_parameters.json', type = str, help='Json file storing all parameter and values for this loop')
    parser.add_argument('-n', '--n_point', default = 2, type = int, help='Number of new points to generate')
    parser.add_argument('-o', '--outfile', default = 'results.json', type = str, help='new point')
    parser.add_argument('-a', '--history', default = 'history.json', type = str, help='all results')
    parser.add_argument('-m', '--max_rounds', default = '4', type = str, help='Maximum loops')
    parser.add_argument('-c', '--current_round', default = '1', type = str, help='Current loop counter')
    parser.add_argument('-p', '--params', nargs='+', default = ['myparamMZD', 'myparamEPSILON'], type = str, help='Name of parameters; should match what is in script_reana.sh \
                                Example: ' \
                                '''
                                sliced_myparamMZD=$2; sliced_myparamEPSILON=$3;
                                echo {\"loop\": ${loop_counter}, \"myparamMZD\": ${sliced_myparamMZD}, \"myparamEPSILON\": ${sliced_myparamEPSILON}} > limit_stage/input_param.json
                                ''')

    main(parser.parse_args())
