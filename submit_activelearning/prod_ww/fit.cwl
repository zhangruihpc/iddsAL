cwlVersion: v1.0
class: Workflow

requirements:
  MultipleInputFeatureRequirement: {}

inputs:
  NTUP: string
  XSEC: string
  BKGH: string

outputs:
  outDS:
    type: string
    outputSource: step_FIT/outDS

steps:

  step_FIT:
    run: prun
    in:
      opt_inDS: NTUP
      opt_secondaryDSs: [BKGH, XSEC]
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/atlas-ghh-analyses/fittingcode:ForRecast-acbf4dc3
      opt_exec:
        default: "echo IN=%IN1 IN0=%IN0; echo pwd; pwd; echo ls -l; ls -l; echo ls -l ..; ls -l ..; echo ls -l ../..; ls -l ../..; mkdir input_tmp/; mv user*ActLearn.root input_tmp/; mv input_tmp ../%{DS1}; mkdir input_tmp/; mv %IN2 input_tmp/; mv input_tmp ../%{SECDS1}; mkdir input_tmp/; cp %IN3 input_tmp/; mv input_tmp ../%{SECDS2}; echo after ls -l ../*; ls -l ../*; echo DS1=%{DS1}  SECDS1=%{SECDS1} SECDS2=%{SECDS2} IN2=%IN2 IN3=%IN3 IN=%IN; source Fit.sh ../%{DS1} ../%{SECDS1}; echo FW, FWW, MASS > myworkdir/point.csv; echo %{sliced_myparamFW}, %{sliced_myparamFWW}, %{sliced_myparamMASS} >> myworkdir/point.csv; echo %{SECDS1}, %{SECDS2}, %{DS1} >> myworkdir/point.csv; cat myworkdir/point.csv; echo ====; cp -r ../%{SECDS2} myworkdir; echo final ls myworkdir; ls myworkdir; ls myworkdir/*; tar cvf myworkdir.tar myworkdir"
      opt_args:
        default: "--maxAttempt 1 --site CERN --useSecrets --outputs 'myworkdir.tar' --avoidVP --forceStaged --forceStagedSecondary --secondaryDSs IN2:2:%{SECDS1},IN3:2:%{SECDS2}"
    out: [outDS]

