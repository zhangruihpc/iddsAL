cwlVersion: v1.0
class: Workflow

requirements:
    MultipleInputFeatureRequirement: {}

inputs:
    signal: string
    background: string

outputs:
    outDS:
        type: string
        outputSource: combine/outDS
  

steps:
    make_signal_eos:
        run: reana
        in:
            opt_inDS: signal
            opt_containerImage:
                default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-submitter
            opt_exec:
                default: "source script_eos.sh from_pchsig_eos %IN"
            opt_args:
                default: "--disableAutoRetry --site  CERN --outputs out-eos-1.tar --useSecrets"
        out: [outDS]

    make_background_dummy:
        run: prun
        in:
            opt_inDS: background
            opt_exec:
                default: "echo make_background_dummy %IN > opq.root; echo make_background_dummy %IN > for_premix.pool"
            opt_args:
                default: "--outputs opq.root,for_premix.pool --nGBPerJob 10"
        out: [outDS]
    
    premix:
        run: prun
        in:
            opt_inDS: make_signal_eos/outDS
            opt_inDsType:
                default: out-eos-1.tar
            opt_secondaryDSs: [make_background_dummy/outDS]
            opt_secondaryDsTypes:
                default: [for_premix.pool]
            opt_exec:
                default: "ls; tar xfv out-eos-1.tar; mv statanalysis/fitresults/limit.png limit-eos.png"
            opt_args:
                default: "--outputs limit-eos.png"
        out: [outDS]
    
    generate_dummy:
        run: prun
        in:
            opt_exec:
                default: "echo %RNDM:10 > gen_dummy.root"
            opt_args:
                default: "--outputs gen_dummy.root --nJobs 1"
        out: [outDS]
      
    make_background_rucio:
        run: reana
        in:
            opt_inDS: background
            opt_containerImage:
                default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-submitter
            opt_exec:
                default: "source script_rucio.sh from_pchsig_rucio"
            opt_args:
                default: "--disableAutoRetry --site  CERN --outputs out-rucio-2.tar --useSecrets"
        out: [outDS]

    combine:
        run: prun
        in:
            opt_inDS: make_signal_eos/outDS
            opt_inDsType:
                default: out-eos-1.tar
            opt_secondaryDSs: [premix/outDS, make_background_rucio/outDS]
            opt_secondaryDsTypes:
                default: [limit-eos.png, out-rucio-2.tar]
            opt_exec:
                default: "tar cvf comb.tar out-eos-1.tar for_premix.pool, out-rucio-2.tar"
            opt_args:
                default: "--outputs comb.tar"
        out: [outDS]
