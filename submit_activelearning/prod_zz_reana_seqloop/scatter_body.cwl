cwlVersion: v1.0
class: Workflow

requirements:
  ScatterFeatureRequirement: {}
  SubworkflowFeatureRequirement: {}

inputs:
  param_myparamMZD:
    type: float[]
    #default: [15, 20, 25, 35, 40, 50, 60]
    #default: [44.66019572067399, 18.686893893167493, 15.59062799889708, 15.791791803370419, 34.39721974233013, 15.476107520860085, 19.9619530154596, 22.26483201791584, 23.157247119940994, 17.587656144624184 ]
    #default: [18.6 , 18.6 , 55.0 , 53.8 , 30.1 , 29.0 , 31.5 , 27.9 , 30.9 , 28.4 , 18.0 , 32.4 , 17.5 , 54.4 , 19.1 , 32.0 , 29.5 , 17.7 , 28.1 , 22.6]
    #default: [18.9 , 37.7 , 37.1, 47.8, 38.5, 47.1]
    default: [38]

  param_myparamEPSILON:
    type: float[]
    #default: [0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001]
    #default: [0.0001061381955643668, 0.0007452991091633345, 0.0009383240628086431, 0.0033397001934474114, 0.000520427628047349, 0.0039319120263095406, 0.005265653414343575, 0.000945982083875787, 0.07788574150006565, 0.0003840633469639008 ]
    #default: [0.00073 , 0.1 , 0.1 , 0.0001 , 0.04 , 0.00035 , 0.0001 , 0.1 , 0.0001 , 0.1 , 0.0001 , 0.04555 , 0.0589 , 0.0001 , 0.1 , 0.1 , 0.0001 , 0.1 , 0.0001 , 0.0522]
    #default: [0.0011 , 0.0013 , 0.00027 , 0.0051 , 0.0104 , 0.00757]
    default: [0.1]


outputs:
  outDS:
    type: string
    outputSource: checkpoint/outDS


steps:
  parallel_work:
    #run: loop_main.cwl
    run: loop_main20.cwl
    #run: loop_main_debug.cwl
    #run: loop_main_small.cwl
    scatter: [param_sliced_myparamMZD, param_sliced_myparamEPSILON]
    scatterMethod: dotproduct
    in:
      param_sliced_myparamMZD: param_myparamMZD
      param_sliced_myparamEPSILON: param_myparamEPSILON
    out: [outDS]


  checkpoint:
    run: junction
    in:
      opt_inDS: parallel_work/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/steeringcontainer:0.0.7
      opt_exec:
        default: "python active_learning_zz.py --max_rounds 1 --current_round %{i}"
      opt_args:
        default: "--site CERN --outputs 'results.json,history.json' --persistentFile history.json --forceStaged"
    out: [outDS]

