#!/usr/bin/env bash
# setup environment
uname -a
alias ls="ls --color=always"

input_file=${1%/}
echo input_file=$input_file

source /home/atlas/release_setup.sh

code=/analysis
#code=/afs/cern.ch/user/z/zhangr/work/HPO/ntuplereader_ghh
ln -s $code/data/ .
echo 'in ntuple.sh ls'
ls
echo 'in ntuple.sh ls *'
ls *

# input_file=../user.zhangr.prod_WWa7_000_step_CxAOD_ActLearn.root
filename1=`\ls $input_file`
input_path_no_root=${input_file%.root}
echo mv $input_file $input_path_no_root
mv $input_file $input_path_no_root
echo in ntuple.sh mv $input_file/$filename1 $input_file/ActLearn.root:
mv $input_path_no_root/$filename1 $input_path_no_root/ActLearn.root

filepath=$input_path_no_root/ActLearn.root
echo in ntuple.sh ls -l $input_path_no_root
ls -l $input_path_no_root
echo in ntuple.sh ls -l ../:
ls -l ../

echo $code/NtupleToHist $(dirname $filepath) ActLearn myworkdir/
$code/NtupleToHist $(dirname $filepath) ActLearn myworkdir/

echo "in ntuple.sh ls myworkdir:"
ls -lht myworkdir

mv myworkdir/output_2Lep_ActLearn.root .
