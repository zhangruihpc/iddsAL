cwlVersion: v1.0
class: Workflow

inputs:
  param_myparamMZD:
    type: float
    default: 80
  param_myparamMHD:
    type: float
    default: 40

outputs:
  outDS:
    type: string
    outputSource: inner_work_bottom/outDS

steps:
  inner_work_evgen:
    run: prun
    in:
      opt_exec:
        default: "export JOBOPTSEARCHPATH=$PWD:$JOBOPTSEARCHPATH; echo 'before' %{i} %{myparamMHD} %{myparamMZD}; ls -l 999999/; sed -i 's/varPARAM_MHD/%{myparamMHD}/g' 999999/mc.MGPy8EG_A14NNPDF23LO_HAHMggfZZd_test.py; sed -i 's/varPARAM_MZD/%{myparamMZD}/g' 999999/mc.MGPy8EG_A14NNPDF23LO_HAHMggfZZd_test.py; date; echo 'before'; ls -l 999999/; echo 'head'; head 999999/mc.MGPy8EG_A14NNPDF23LO_HAHMggfZZd_test.py; echo 'check env'; export envPARAM_MHD=%{myparamMHD}; export envPARAM_MZD=%{myparamMZD}; echo $envPARAM_MHD $envPARAM_MZD; echo 'check env2'; python -c 'import os; print(os.environ[\"envPARAM_MZD\"]); print(os.environ[\"envPARAM_MHD\"])'; echo something > EVNT.pool.root.1; cat EVNT.pool.root.1"
      opt_args:
        default: "--disableAutoRetry --outputs 'EVNT.pool.root.1'"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_simul:
    run: prun
    in:
      opt_inDS: inner_work_evgen/outDS
      opt_exec:
        default: "echo something > HITS.pool.root.1; echo HITS.pool.root.1"
      opt_args:
        default: "--outputs 'HITS.pool.root.1'"
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_reco:
    run: prun
    in:
      opt_inDS: inner_work_simul/outDS
      opt_exec:
        default: "echo something > AOD.pool.root.1; echo AOD.pool.root.1"
      opt_args:
        default: "--outputs 'AOD.pool.root.1' "
      opt_useAthenaPackages:
        default: true
    out: [outDS]

  inner_work_deriv:
    run: prun
    in:
      opt_inDS: inner_work_reco/outDS
      opt_exec:
        default: "echo something> DAOD_HIGG2D1.pool.root.1; echo DAOD_HIGG2D1.pool.root.1"
      opt_args:
        default: "--outputs 'DAOD_HIGG2D1.pool.root.1' "
      opt_useAthenaPackages:
        default: true
    out: [outDS]


  inner_work_reana:
    run: reana
    in:
      opt_inDS: inner_work_deriv/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/iddsal:atlas-reana-submitter
      opt_exec:
        default: "echo DS0 = %{DS0}; source script_rucio.sh pchain_ZZ %{DS0} "
      opt_args:
        default: "--disableAutoRetry --site  CERN --outputs out-rucio-2.tar --useSecrets"
    out: [outDS] 


  checkpoint:
    run: junction
    in:
      opt_inDS: inner_work_reana/outDS
      opt_containerImage:
        default: docker://gitlab-registry.cern.ch/zhangruihpc/steeringcontainer:0.0.7
      opt_exec:
        default: "echo DS0aaaa %{DS0} DS1aaaa %{DS1}; bash script_junction.sh %{i} %{DS0} %{myparamMZD} %{myparamMHD}"
      opt_args:
        default: "--disableAutoRetry --site  CERN --outputs 'results.json,history.json' --persistentFile history.json --forceStaged"
    out: [outDS]

